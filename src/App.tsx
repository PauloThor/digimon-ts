import { useEffect, useState } from "react";
import { Container, List, FavoritesList } from "./styles";
import "./App.css";
import Digimons from "./components/Digimons";
import { useFavoriteDigimons } from "./provider/digimons";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

const App = () => {
  const [digimons, setDigimons] = useState<Digimon[]>([]);

  const { favorites } = useFavoriteDigimons();

  useEffect(() => {
    fetch("https://digimon-api.vercel.app/api/digimon")
      .then((response) => response.json())
      .then((response) => setDigimons([...response]))
      .catch(() => console.log("Algo de errado!"));
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <Container>
          <FavoritesList>
            <Digimons digimons={favorites} isFavoritesList={true}></Digimons>
          </FavoritesList>
          <List>
            <Digimons digimons={digimons}></Digimons>
          </List>
        </Container>
      </header>
    </div>
  );
};

export default App;
