import { ReactNode } from "react";
import { FavoriteDigimonsProvider } from "./digimons";

interface FavoriteDigimonsProviderProps {
  children: ReactNode;
}

const Provider = ({ children }: FavoriteDigimonsProviderProps) => {
  return <FavoriteDigimonsProvider>{children}</FavoriteDigimonsProvider>;
};

export default Provider;
