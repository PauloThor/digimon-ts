import { Container, Image } from "./styles";
import Button from "../Button";
import { useFavoriteDigimons } from "../../provider/digimons";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

interface DigimonCardProps {
  digimon: Digimon;
  isFavoritesList: boolean;
}

const DigimonCard = ({ digimon, isFavoritesList }: DigimonCardProps) => {
  const { name, level, img } = digimon;

  const { addDigimon, deleteDigimon } = useFavoriteDigimons();

  return (
    <Container>
      <div>{name}</div>
      <Image src={img}></Image>
      <div>{level}</div>
      {isFavoritesList ? (
        <Button deleted={true} onClick={() => deleteDigimon(digimon)}>
          Remove
        </Button>
      ) : (
        <Button onClick={() => addDigimon(digimon)}>Add</Button>
      )}
    </Container>
  );
};

export default DigimonCard;
