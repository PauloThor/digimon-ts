import DigimonCard from "../CardDigimon";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

interface DigimonProps {
  isFavoritesList?: boolean;
  digimons: Digimon[];
}

const Digimons = ({ digimons, isFavoritesList }: DigimonProps) => {
  return (
    <>
      {digimons.map((digimon, index) => (
        <DigimonCard
          key={index}
          digimon={digimon}
          isFavoritesList={isFavoritesList || false}
        ></DigimonCard>
      ))}
    </>
  );
};

export default Digimons;
