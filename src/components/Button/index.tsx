import { ReactNode } from "react";
import { ButtonStyled } from "./styles";

interface ButtonProps {
  deleted?: boolean;
  onClick: () => void;
  children: ReactNode;
}

const Button = ({ deleted, children, onClick }: ButtonProps) => {
  return (
    <ButtonStyled deleted={deleted || false} onClick={onClick}>
      {children}
    </ButtonStyled>
  );
};

export default Button;
