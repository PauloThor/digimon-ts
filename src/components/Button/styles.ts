import styled, { css } from "styled-components";

interface ButtonStyledProps {
  deleted: boolean;
}

export const ButtonStyled = styled.button<ButtonStyledProps>`
  margin-top: 5px;
  width: 48px;
  font-size: 12px;
  height: 22px;
  border: none;
  border-radius: 10px;
  background-color: deepskyblue;
  outline: none;
  cursor: pointer;

  ${(props) =>
    props.deleted &&
    css`
      background: white;
      color: red;
      width: 52px;
      display: flex;
      justify-content: center;
      align-items: center;
    `};
`;
